# Minionese

A Minionese translator inspired by the movie, "Minions."

Originally written by Danbrown (parasane@gmail.com) at noon on 1 July, 2016,
and (at least prior to initial commit to GitLab on Sunday, 3 May, 2020, or
COVID-19 QDatePA[Lackawanna] 37), it had last been updated on 4 June, 2017.
