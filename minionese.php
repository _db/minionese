<?php
/**
 * Written on the final full day of the Davis/Philips/Lindsey/Berkowitz/Young/Brown
 * 2016 bi-annual Virginia Beach family excursion.  Written from the front room,
 * looking out the left sliding door (left from the inside) toward the ocean, at
 * noon on 1 July, 2016, while the families were enjoying the beach and I was, once
 * again, stuck working (and waiting for results to come back from a security scan).
 * Our house at 3349 Sandfiddler (the second house being 3361) is now filling up
 * again as everyone treks back for lunch.  All of this is just for my own edification,
 * and has nothing to do with the small vodka and Arnold Palmer mix I just made,
 * since I just had my first sip a moment ago.  And don't sit there and judge me,
 * Future Dan or other person --- Allison and her boyfriend, Justin, picked up a
 * brand-new handle of Kettle One vodka last night, and it's our challenge - nay, our
 * duty - to finish it before the long drive home tomorrow.  I really do love to
 * drive, and for long distances in unfamiliar or semi-familiar territory, but only
 * once I've been underway for twenty-five minutes or more.  Prior to that point,
 * I dread the long trip and would rather just stay-put wherever I may happen to be.
 */
error_reporting(0);

$dictfile = dirname(__FILE__).'/minionese.dict';

$dict_lines = explode(PHP_EOL,file_get_contents($dictfile));

foreach ($dict_lines as $line) {

	if (strstr($line,'#')) next;

	$terms[] = preg_replace('/(\t)\\1+/',',',$line);

}

if (php_sapi_name() == 'cli') {
	$input_phrase = $argv[1];
} else {
	$input_phrase = $_POST['input_phrase'];
}

foreach ($terms as $term) {
	$t = explode(',',$term);
	$input_phrase = preg_replace('/\b'.strtolower($t[0]).'\b/',strtolower($t[1]),strtolower($input_phrase));
}

echo strtoupper($input_phrase).PHP_EOL;
